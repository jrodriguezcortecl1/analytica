Flask==0.11
Jinja2==2.8
gunicorn==19.6.0
Flask-Bootstrap==3.3.7
psycopg2
flask_sqlalchemy==2.1
flask-login==0.4.1
WTForms==2.3.1
APScheduler==3.0.0
pandas
numpy
matplotlib
seaborn
pickle-mixin
sklearn
google-cloud-bigquery
google-oauth
flask_paginate
