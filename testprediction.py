import pandas as pd # version 1.0.5
import numpy as np  # version 1.17.2 
import matplotlib.pyplot as plt # version 3.1.1
import seaborn as sns # version 0.9.0
import pickle # version 4.0
from sklearn.metrics import classification_report,confusion_matrix  # version 0.9.0 
from sklearn.cluster import KMeans  # version 0.9.0
from sklearn.ensemble import GradientBoostingClassifier # version 0.9.0 
from sklearn.tree import DecisionTreeClassifier # version 0.9.0 
from sklearn.ensemble import RandomForestClassifier # version 0.9.0 
from sklearn.linear_model import LogisticRegression # version 0.9.0 
from sklearn.ensemble import AdaBoostClassifier # version 0.9.0 

df_user = pd.read_csv('files/generate.csv').drop(columns='Unnamed: 0')
X, y = df_user.drop(['ProximaCompra','NextPurchaseDayRange','user_id'],axis=1), df_user.NextPurchaseDayRange
bestmodel = pickle.load(open('files/bestmodel.pickle','rb'))
y_hat = bestmodel.predict(X) 
X['prediccion'] = y_hat
X['user_id'] = df_user['user_id']
X.loc[X.prediccion == 2, 'prediccion'] = 'Alto'
X.loc[X.prediccion == 1, 'prediccion'] = 'Medio'
X.loc[X.prediccion == 0, 'prediccion'] = 'Bajo'
X = X[['user_id', 'prediccion', 'Recency', 'Frequency', 'Revenue', 'DayDiff', 'DayDiff2', 'DayDiff3', 'DayDiffMean', 'DayDiffStd']]
X.columns = ['Usuario', 'Predicción', 'Recencia', 'Frecuencia', 'Money', 'DayDiff', 'DayDiff2', 'DayDiff3', 'DayDiffMean', 'DayDiffStd']
print(X.shape)

