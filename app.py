from flask import Flask, request, redirect, Response, render_template, flash, session, url_for
from flask import send_file
from flask_bootstrap import Bootstrap
from flask_login import LoginManager,login_user,logout_user,login_required,current_user
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField
from shared import db
from models import *
from google.cloud import bigquery
from google.oauth2.service_account import Credentials
import csv
import time
import os
from os import path
from shutil import copyfile
from datetime import datetime, timedelta,date
from flask import Flask, render_template
from flask_paginate import Pagination, get_page_args
import pandas as pd # version 1.0.5
import numpy as np  # version 1.17.2 
import matplotlib.pyplot as plt # version 3.1.1
import seaborn as sns # version 0.9.0
import pickle # version 4.0
from sklearn.metrics import classification_report,confusion_matrix  # version 0.9.0 
from sklearn.cluster import KMeans  # version 0.9.0
from sklearn.ensemble import GradientBoostingClassifier # version 0.9.0 
from sklearn.tree import DecisionTreeClassifier # version 0.9.0 
from sklearn.ensemble import RandomForestClassifier # version 0.9.0 
from sklearn.linear_model import LogisticRegression # version 0.9.0 
from sklearn.ensemble import AdaBoostClassifier # version 0.9.0 
from sklearn.model_selection import train_test_split # version 0.9.0 


# Settings
app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'XXXXXXXXXXXXXX'
app.secret_key = 'XXXXXXXXXXXXXXXXX'
Bootstrap(app)
db.init_app(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

# Función que se encarga de realizar el inicio de sesion
@app.route("/", methods=['GET', 'POST'])
def login():
    form = request.form

    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        user = validate(username,password)
        if user == 0:
            flash('Error: Usuario o contraseña inválidos') 
            return render_template('login.html', form=form)
        else:
            login_user(user)
            return redirect(url_for('index'))           
    else:
         return render_template('login.html', form=form)

# Función que muestra la pantalla de inicio
@app.route('/inicio')
@login_required
def index():
    return render_template('index.html')
  
# Función que accede a la página de transformación
@app.route('/transformacion')
@login_required
def transformacion(): 
    return render_template('transforma.html')

# Función que realiza la descarga de los datos
@app.route('/descargardatos')
@login_required
def descargardatos(): 
    return send_file('files/generate.csv', as_attachment=True)

# Función que realiza la descarga de los resultados
@app.route('/descargarresultados')
def descargarresultados():
    df = pd.read_csv('files/resultados.csv')
    timestr = time.strftime("%Y%m%d-%H%M%S")
    filename = 'descarga_resultados_'+str(timestr)+'.csv'
    df.to_csv('files/'+str(filename))
    return send_file('files/resultados.csv',
                     mimetype='text/csv',
                     attachment_filename=filename,
                     as_attachment=True, cache_timeout=0)

# Función que accede a la página de predicción
@app.route("/prediccion", methods=['GET', 'POST'])
@login_required
def prediccion():
    shape = ''
    if request.method == 'POST':
        if request.files['file'].filename == '':
            df = pd.read_csv('files/generate.csv')
            shape = df.columns
        else:
            df = pd.read_csv(request.files.get('file'))
            shape = df.columns
        print(df.columns)
        columnas_archivo  = list(df.columns) 
        listado_columnas = ['Unnamed: 0', 'user_id', 'Recency', 'Frequency', 'Revenue', 'DayDiff', \
       			     'DayDiff2', 'DayDiff3', 'DayDiffMean', 'DayDiffStd'] 
        if set([ 'user_id', 'Recency', 'Frequency', 'Revenue', \
                  'DayDiff', 'DayDiff2', 'DayDiff3', 'DayDiffMean', 'DayDiffStd']).issubset(df.columns) and (columnas_archivo == listado_columnas):
            df_prediccion = proceso_prediccion(df)
            global data
            data = df_prediccion.to_dict(orient = 'records')
            return redirect(url_for('resultados'))
        else:
            flash('Error: El archivo no posee la estructura de columnas requerida para el estudio \n Por favor descargue el archivo desde la sección Transforma')     
    return render_template('prediccion.html',shape=shape)

# Función que realiza el proceso de prediccion de los datos
@app.route('/proceso_prediccion')
@login_required
def proceso_prediccion(df_user):
    df_user = df_user.drop(columns='Unnamed: 0')
    X = df_user.drop(['user_id'],axis=1)
    bestmodel = pickle.load(open('files/bestmodel.pickle','rb'))
    y_hat = bestmodel.predict(X) 
    X['prediccion'] = y_hat
    X['user_id'] = df_user['user_id']
    X.loc[X.prediccion == 2, 'prediccion'] = 'entre 0 a 15 días'
    X.loc[X.prediccion == 1, 'prediccion'] = 'entre 16 a 30 días'
    X.loc[X.prediccion == 0, 'prediccion'] = 'mayor a 30 días'
    df = X[['user_id', 'prediccion', 'Recency', 'Frequency', 'Revenue', 'DayDiff', 'DayDiff2', 'DayDiff3', 'DayDiffMean', 'DayDiffStd']]
    df['DayDiffMean'] = df['DayDiffMean'].apply(lambda x: round(x, 2))
    df['DayDiffStd'] = df['DayDiffStd'].apply(lambda x: round(x, 2))
    # Guardar los resultados de la predicción 
    df.to_csv('files/resultados.csv')

    return df

# Función para el proceso de paginación del dataframe
def get_data(data, offset=0, per_page=10):
    return data[offset: offset + per_page]


@app.route('/resultados')
@login_required
# Creación de la tabla a visualizar en la vista
def resultados():
    df_prediccion = pd.read_csv('files/resultados.csv')   
    data = df_prediccion.to_dict(orient = 'records')
    page, per_page, offset = get_page_args(page_parameter='page',
                                           per_page_parameter='per_page')
    total = len(data)
    pagination_users = get_data(data, offset=offset, per_page=per_page)
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
    return render_template('resultados.html',
                           users=pagination_users,
                           page=page,
                           per_page=per_page,
                           pagination=pagination,
                           shape=total
                           )

# Función que accede a la página de visualización
@app.route('/visualizacion')
@login_required
def visualizacion():
    return render_template('visualiza.html')

# Función que se encarga de realizar el cierre de sesión
@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login')) 

# Función que se encarga de verificar que el usuario y la contraseña se encuentren
# registrados en la base de datos
@login_manager.user_loader
def load_user(user_id):
    return User.query.filter_by(account_id=user_id).first()

# Función que se encarga de validar el usuario y el password a nivel de base de datos
def validate(username,password):
    user = User.query.filter(User.username == username).filter(User.userpass == password).first()
    if user is None:
       return 0
    else:
       return user

# Función de prueba que verifica el retorno de una muestra proveniente de BigQuery
@app.route('/testretorno')
def testretorno():

    print('Credendtials from environ: {}'.format(os.environ.get('GOOGLE_APPLICATION_CREDENTIALS')))

    scopes = ('https://www.googleapis.com/auth/bigquery',
              'https://www.googleapis.com/auth/cloud-platform',
              'https://www.googleapis.com/auth/drive')
    credentials = Credentials.from_service_account_file('latamcredentials.json')
    credentials = credentials.with_scopes(scopes)
    client = bigquery.Client(credentials=credentials)

    querysql = 'SELECT * FROM deep-mechanism-282313.recargafacil.historico limit 20'
    df = client.query(querysql).result().to_dataframe()
    
    return render_template('testretorno.html',  tables=[df.to_html(classes='data')], titles=df.columns.values)

# Inicio de la aplicación
if __name__ == '__main__':
   app.run()

