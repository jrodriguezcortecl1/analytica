from google.cloud import bigquery
from google.oauth2.service_account import Credentials
import pandas as pd
import csv
import time
from datetime import datetime, timedelta,date
import pandas as pd # version 0.25.1 
import numpy as np  # version 1.17.2 
import matplotlib.pyplot as plt # version 3.1.1
import seaborn as sns # version 0.9.0
import pickle # version 4.0
from sklearn.metrics import classification_report,confusion_matrix  # version 0.9.0 
from sklearn.cluster import KMeans  # version 0.9.0


def cronjob():
    """
    Main cron job.
    The main cronjob to be run continuously.
    """
    print('Inicio')
    scopes = ('https://www.googleapis.com/auth/bigquery',
	      'https://www.googleapis.com/auth/cloud-platform',
	      'https://www.googleapis.com/auth/drive')
    credentials = Credentials.from_service_account_file('latamcredentials.json')
    credentials = credentials.with_scopes(scopes)
   
    client = bigquery.Client(credentials=credentials)

    querysql = 'SELECT * FROM deep-mechanism-282313.recargafacil.historico'
    df = client.query(querysql).result().to_dataframe()

    df['user_id'].replace('1B2M2Y8AsgTpgAmY7PhCfg==', np.NaN, inplace=True)
    df=df.dropna()
    df['trx_date']=pd.to_datetime(df['trx_date'])
    df_hist = df[(df.trx_date < datetime(2020,5,1)) & (df.trx_date >= datetime(2019,1,1))].reset_index(drop=True)
    df_next = df[(df.trx_date >= datetime(2020,5,1)) & (df.trx_date < datetime(2020,5,31))].reset_index(drop=True)

    df_user = pd.DataFrame(df_hist['user_id'].unique())
    df_user.columns = ['user_id']

    ultima_compra = df_hist.groupby('user_id').trx_date.max().reset_index()
    ultima_compra.columns = ['user_id','FechaMaxTrx']

    primera_compra = df_next.groupby('user_id').trx_date.min().reset_index()
    primera_compra.columns = ['user_id','FechaMinTrx']

    df_compras = pd.merge(ultima_compra,primera_compra,on='user_id',how='left')

    df_compras['ProximaCompra'] = (df_compras['FechaMinTrx'] - df_compras['FechaMaxTrx']).dt.days

    df_user = pd.merge(df_user, df_compras[['user_id','ProximaCompra']],on='user_id',how='left')

    df_user = df_user.dropna()

    df_recency = df_hist.groupby('user_id').trx_date.max().reset_index()
    df_recency.columns = ['user_id','FechaMaxTrx']

    df_recency['Recency'] = (df_recency['FechaMaxTrx'].max() - df_recency['FechaMaxTrx']).dt.days
    df_user = pd.merge(df_user, df_recency[['user_id','Recency']], on='user_id')

    df_frequency = df_hist.groupby('user_id').trx_date.count().reset_index()
    df_frequency.columns = ['user_id','Frequency']

    df_user = pd.merge(df_user, df_frequency, on='user_id')

    df_hist['Revenue'] = df_hist['amount'] 
    df_revenue = df_hist.groupby('user_id').Revenue.sum().reset_index()

    df_user = pd.merge(df_user, df_revenue, on='user_id')

    df_diasCompra = df_hist[['user_id','trx_date']]

    df_diasCompra = df_diasCompra.sort_values(['user_id','trx_date'])
    df_diasCompra = df_diasCompra.drop_duplicates(subset=['user_id','trx_date'],keep='first')

    df_diasCompra['TrxFechaPrevia'] = df_diasCompra.groupby('user_id')['trx_date'].shift(1)
    df_diasCompra['Trx2FechaPrevia'] = df_diasCompra.groupby('user_id')['trx_date'].shift(2)
    df_diasCompra['Trx3FechaPrevia'] = df_diasCompra.groupby('user_id')['trx_date'].shift(3)
    df_diasCompra.head()

    df_diasCompra['DayDiff'] = (df_diasCompra['trx_date'] - df_diasCompra['TrxFechaPrevia']).dt.days
    df_diasCompra['DayDiff2'] = (df_diasCompra['trx_date'] - df_diasCompra['Trx2FechaPrevia']).dt.days
    df_diasCompra['DayDiff3'] = (df_diasCompra['trx_date'] - df_diasCompra['Trx3FechaPrevia']).dt.days

    df_dias_diff = df_diasCompra.groupby('user_id').agg({'DayDiff': ['mean','std']}).reset_index()
    df_dias_diff.columns = ['user_id', 'DayDiffMean','DayDiffStd']
    df_diasUltimasCompra = df_diasCompra.drop_duplicates(subset=['user_id'],keep='last')

    df_diasUltimasCompra = df_diasUltimasCompra.dropna()
    df_diasUltimasCompra = pd.merge(df_diasUltimasCompra, df_dias_diff, on='user_id')

    df_user = pd.merge(df_user, df_diasUltimasCompra[
		                            ['user_id','DayDiff','DayDiff2','DayDiff3',
		                             'DayDiffMean','DayDiffStd']], on='user_id')
    df_user.drop(['ProximaCompra','NextPurchaseDayRange'],axis=1) 
#    df_user['NextPurchaseDayRange'] = 2
#    df_user.loc[df_user.ProximaCompra>15,'NextPurchaseDayRange'] = 1
#    df_user.loc[df_user.ProximaCompra>30,'NextPurchaseDayRange'] = 0
    
    t = time.localtime()
    timestamp = time.strftime('%Y-%m-%d', t)
    nombre_archivo = "backup_" + timestamp

    df_user.to_csv(str(nombre_archivo)+'.csv')

    t = time.localtime()
    timestamp = time.strftime('%Y-%m-%d', t)
    nombre_archivo = "backup_" + timestamp
    df.to_csv('/app/'+str(nombre_archivo)+'.csv')

    print(df.head(5))
    print('Cierre del proceso')
