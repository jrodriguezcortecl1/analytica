# Package Scheduler.
from apscheduler.schedulers.blocking import BlockingScheduler

# Main cronjob function.
from transformacion import cronjob

# Create an instance of scheduler and add function.
scheduler = BlockingScheduler()

scheduler.add_job(cronjob, trigger='cron', hour='1', minute='10')

scheduler.start()
