from google.cloud import bigquery
from google.oauth2.service_account import Credentials
import pandas as pd
import csv
import time
import os
from datetime import datetime, timedelta,date
import pandas as pd # version 0.25.1 
import numpy as np  # version 1.17.2 
import matplotlib.pyplot as plt # version 3.1.1
import seaborn as sns # version 0.9.0
import pickle # version 4.0
from sklearn.metrics import classification_report,confusion_matrix  # version 0.9.0 
from sklearn.cluster import KMeans  # version 0.9.0

print('Credendtials from environ: {}'.format(os.environ.get('GOOGLE_APPLICATION_CREDENTIALS')))

scopes = ('https://www.googleapis.com/auth/bigquery',
      'https://www.googleapis.com/auth/cloud-platform',
      'https://www.googleapis.com/auth/drive')
credentials = Credentials.from_service_account_file('latamcredentials.json')
credentials = credentials.with_scopes(scopes)

client = bigquery.Client(credentials=credentials)

querysql = 'SELECT * FROM deep-mechanism-282313.recargafacil.historico limit 20'
df = client.query(querysql).result().to_dataframe()

print(df.shape)

