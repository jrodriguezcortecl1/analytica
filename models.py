from shared import db

class TypeUser(db.Model):
    __tablename__ = 'type_user'

    type_user_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)

    def __repr__(self):
        return '<id {}>'.format(self.type_user_id)

class User(db.Model):
    __tablename__ = 'account'

    account_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True)
    userpass = db.Column(db.String(50))
    type_user_id = db.Column(db.Integer)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.account_id)   

    def __repr__(self):
        return '<id {}>'.format(self.username)



